#include "FastLED.h"

#define SENSOR_PIN A0
#define SENSITIVITY_PIN A2
#define BRIGHTNESS_PIN_A A6
#define BRIGHTNESS_PIN_B A4
#define NUM_LEDS_A 100
#define NUM_LEDS_B 50
#define NUM_STRIPS 2

CRGB leds[NUM_LEDS_A];
CRGB safety[NUM_LEDS_B];
CLEDController *controllers[NUM_STRIPS];

unsigned int hue = 0;       // the starting color of the rainbow strips
unsigned int randNum = 255; // the starting color of the reactive strips
unsigned int tick = 0;      // time-keeping

unsigned int deadCount = 96; // this is how many LEDs we want to be black
unsigned int sample;         // the input from the sound sensor
unsigned int stableSample;
unsigned int stableNum = 0; // how much to smooth the sample;

void setup()
{
  // pause to allow electrons to accumulate
  delay(1000);
  // controllers array for multiple scenes
  controllers[0] = &FastLED.addLeds<WS2811, 6, BRG>(leds, NUM_LEDS_A);
  controllers[1] = &FastLED.addLeds<WS2811, 4, BRG>(safety, NUM_LEDS_B);

  pinMode(BRIGHTNESS_PIN_A, INPUT); // Set brightness for display leds
  pinMode(BRIGHTNESS_PIN_B, INPUT); // Set brightness for safety leds
  pinMode(SENSOR_PIN, INPUT);       // Set the sound sensor pin as input
  pinMode(SENSITIVITY_PIN, INPUT);  // Set sensitivity pot adjust as input

  Serial.begin(9600);
  // and breathe...
  delay(1000);
}

void loop()
{
  // manual sensitivity setting
  unsigned int sensitivityInput = analogRead(SENSITIVITY_PIN);
  unsigned int sensitivity = map(sensitivityInput, 0, 1023, 1, 10);

  // grab a sample of the sound
  sample = analogRead(SENSOR_PIN);

  // stabilize the sample to reduce the jitters
  if (sample > stableSample + stableNum || sample < stableSample - stableNum)
  {
    stableSample = sample;
  }

  // map the safety strips brightness to the deadCount
  unsigned int soundBrightness = map(deadCount, 96, 0, 25, 100);

  // involve the sensitivity
  stableSample *= sensitivity;
  // but then limit the range
  if (stableSample < 10)
  {
    stableSample = 10;
  }
  if (stableSample > 700)
  {
    stableSample = 700;
  }

  deadCount = map(stableSample, 10, 700, NUM_LEDS_A - 4, 0);
  // alter deadCount with sensitivity
  deadCount -= sensitivity;

  // manual brightness controls
  unsigned int aBrightInput = analogRead(BRIGHTNESS_PIN_A);
  unsigned int displayBrightness = map(aBrightInput, 0, 1023, 16, 255);
  unsigned int bBrightInput = analogRead(BRIGHTNESS_PIN_B);
  unsigned int safetyBrightness = map(bBrightInput, 0, 1023, 16, 255);

  // clear the show
  FastLED.clear();

  // start the show

  // if sensitivity is cranked up, put on the white lights
  if (sensitivity >= 10)
  {
    unsigned int safeBright = 185;
    fill_solid(leds, NUM_LEDS_A, CRGB::White);
    fill_solid(safety, NUM_LEDS_B, CRGB::Yellow);
    controllers[0]->showLeds(safeBright);
    controllers[1]->showLeds(safeBright);
  }
  // else start the party
  else
  {
    // design the show
    fill_solid(leds, NUM_LEDS_A, CHSV(randNum, 255, 255));
    fill_solid(leds, deadCount, CRGB::Black);

    // draw led data for the display strips into LEDs
    controllers[0]->showLeds(displayBrightness);

    // design the safety strips
    fill_rainbow(safety, NUM_LEDS_B, hue, 5);
    // draw led data for the safety strips into leds
    controllers[1]->showLeds(soundBrightness);

    // add a count for timing stuff
    if (tick % 2 == 0)
    {
      hue += 3;
    }
    if (tick % 10 == 0)
    {
      randNum = random8();
    }
    tick++;
    if (tick > 100000000)
    {
      tick = 0;
    }
  }

  // Serial print stuff for debug:

  // Serial.print("DEADCOUNT: ");
  // Serial.println(deadCount);
  // Serial.print("SENSITIVITY: ");
  // Serial.println(sensitivity);
  // Serial.print("soundBrightness: ");
  // Serial.println(soundBrightness);

  // Serial.print("Bright A: ");
  // Serial.println(displayBrightness);
  // Serial.print("Bright B: ");
  // Serial.println(safetyBrightness);
}
