# led show

## Overall Goals:

This project will supply a box that will plug in to 120VAC wall power and control 6 led strips that are each ~7.5 meters long. Initially, the led strips will have light starting at the end of strip farthest from the control box. The led strips will light up more (more lit leds along the length of the strip) in response to sound – the louder the sound, the more of the strip will be lit. The default light pattern will be fast succession random colors.

The control box will have the following controls:
• Brightness: controls the overall brightness (and power draw) of the led strips
• Sensitivity: controls how reactive the led strips are to sound input

The overall gist is as follows:
• The 120VAC → 12VDC converter will plug into wall power and supply 200W 12VDC+ and Ground to buss bars in the control box.
• A lead from the 12VDC+ buss will connect to a 12VDC → 5VDC converter
◦ this converter will then connect to a 5VDC+ buss
▪ this buss will power the control knobs and sound sensor
• The 6 led show strips will connect to the 12VDC+, Ground Buss and Signal Buss respectively
• The 4 led safety strips will connect to the 12VDC+, Ground Buss and Signal Buss respectively
• The Arduino Nano will act as the brains of the project
◦ Inputs:
▪ Sound decibel sensor
▪ Brightness control (show)
▪ Brightness control (safety)
▪ Sensitivity control

◦ Outputs:
▪ LED Show strips signal buss
▪ LED Safety strips signal buss
